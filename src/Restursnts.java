import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.DefaultComboBoxModel;

public class Restursnts {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Restursnts window = new Restursnts();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Restursnts() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 255, 204));
		frame.setBackground(new Color(255, 255, 255));
		frame.setForeground(new Color(255, 0, 255));
		frame.setBounds(0, 0, 1600, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Resturant Management System");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 50));
		lblNewLabel.setBounds(412, 63, 741, 74);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnAdmin = new JButton("Admin");
		btnAdmin.setBackground(new Color(0, 0, 128));
		btnAdmin.setForeground(new Color(240, 255, 255));
		btnAdmin.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAdmin.setBounds(67, 63, 155, 86);
		frame.getContentPane().add(btnAdmin);
		
		JButton btnChef = new JButton("Chef");
		btnChef.setBackground(new Color(0, 0, 128));
		btnChef.setForeground(new Color(240, 255, 255));
		btnChef.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnChef.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnChef.setBounds(1266, 75, 155, 86);
		frame.getContentPane().add(btnChef);
		
		JPanel BreakfirstPanal = new JPanel();
		BreakfirstPanal.setBackground(new Color(255, 255, 0));
		BreakfirstPanal.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		BreakfirstPanal.setBounds(107, 210, 333, 404);
		frame.getContentPane().add(BreakfirstPanal);
		BreakfirstPanal.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Breakfirst");
		lblNewLabel_1.setForeground(new Color(128, 0, 0));
		lblNewLabel_1.setBounds(105, 9, 122, 52);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 25));
		BreakfirstPanal.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Noodles");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_2.setBounds(21, 81, 150, 52);
		BreakfirstPanal.add(lblNewLabel_2);
		
		JLabel lblNewLabel_2_1 = new JLabel("Stringhoppers");
		lblNewLabel_2_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_2_1.setBounds(21, 151, 185, 46);
		BreakfirstPanal.add(lblNewLabel_2_1);
		
		JLabel lblNewLabel_2_1_1 = new JLabel("Rice and curry");
		lblNewLabel_2_1_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_2_1_1.setBounds(21, 219, 173, 52);
		BreakfirstPanal.add(lblNewLabel_2_1_1);
		
		JTextArea textAreaNoodls = new JTextArea();
		textAreaNoodls.setBackground(new Color(255, 255, 255));
		textAreaNoodls.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaNoodls.setBounds(181, 99, 142, 22);
		BreakfirstPanal.add(textAreaNoodls);
		
		JTextArea textAreaStringhoppers = new JTextArea();
		textAreaStringhoppers.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaStringhoppers.setBounds(181, 161, 142, 22);
		BreakfirstPanal.add(textAreaStringhoppers);
		
		JTextArea textAreaRiceandcurry = new JTextArea();
		textAreaRiceandcurry.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaRiceandcurry.setBounds(181, 232, 142, 22);
		BreakfirstPanal.add(textAreaRiceandcurry);
		
		JLabel lblNewLabel_5 = new JLabel("Drink");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_5.setBounds(33, 299, 76, 22);
		BreakfirstPanal.add(lblNewLabel_5);
		
		JComboBox B_comboBox = new JComboBox();
		B_comboBox.setFont(new Font("Tahoma", Font.BOLD, 15));
		B_comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select Drink", "Tea", "Coffee", "Ice Coffee", "Water bottle"}));
		B_comboBox.setBounds(10, 340, 132, 21);
		BreakfirstPanal.add(B_comboBox);
		
		JLabel lblNewLabel_6 = new JLabel("Qty");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_6.setBounds(219, 299, 69, 22);
		BreakfirstPanal.add(lblNewLabel_6);
		
		JTextArea textAreaB_drink = new JTextArea();
		textAreaB_drink.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaB_drink.setBounds(190, 338, 98, 22);
		BreakfirstPanal.add(textAreaB_drink);
		
		JPanel LunchPanal = new JPanel();
		LunchPanal.setBackground(new Color(255, 255, 0));
		LunchPanal.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		LunchPanal.setBounds(575, 210, 333, 404);
		frame.getContentPane().add(LunchPanal);
		LunchPanal.setLayout(null);
		
		JLabel lblNewLabel_1_1 = new JLabel("Lunch");
		lblNewLabel_1_1.setForeground(new Color(128, 0, 0));
		lblNewLabel_1_1.setBounds(129, 9, 75, 31);
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.BOLD, 25));
		LunchPanal.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_3 = new JLabel("Rice and curry");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel_3.setBounds(10, 76, 209, 47);
		LunchPanal.add(lblNewLabel_3);
		
		JLabel lblNewLabel_3_1 = new JLabel("Chicken");
		lblNewLabel_3_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_3_1.setBounds(35, 133, 120, 31);
		LunchPanal.add(lblNewLabel_3_1);
		
		JLabel lblNewLabel_3_2 = new JLabel("Vegittable");
		lblNewLabel_3_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_3_2.setBounds(35, 235, 128, 31);
		LunchPanal.add(lblNewLabel_3_2);
		
		JLabel lblNewLabel_3_1_1 = new JLabel("Egg");
		lblNewLabel_3_1_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_3_1_1.setBounds(35, 183, 81, 31);
		LunchPanal.add(lblNewLabel_3_1_1);
		
		JTextArea textAreaChicken = new JTextArea();
		textAreaChicken.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaChicken.setBounds(173, 133, 141, 22);
		LunchPanal.add(textAreaChicken);
		
		JTextArea textAreaEgg = new JTextArea();
		textAreaEgg.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaEgg.setBounds(173, 190, 141, 22);
		LunchPanal.add(textAreaEgg);
		
		JTextArea textAreaVegetable = new JTextArea();
		textAreaVegetable.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaVegetable.setBounds(173, 242, 141, 22);
		LunchPanal.add(textAreaVegetable);
		
		JLabel lblNewLabel_5_1 = new JLabel("Drink");
		lblNewLabel_5_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_5_1.setBounds(35, 311, 76, 22);
		LunchPanal.add(lblNewLabel_5_1);
		
		JComboBox L_comboBox = new JComboBox();
		L_comboBox.setFont(new Font("Tahoma", Font.BOLD, 15));
		L_comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select Drink", "Cream Soda", "Fanta", "Sprite", "Coke", "EGB", "Water bottel"}));
		L_comboBox.setBounds(10, 346, 132, 21);
		LunchPanal.add(L_comboBox);
		
		JLabel lblNewLabel_6_1 = new JLabel("Qty");
		lblNewLabel_6_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_6_1.setBounds(229, 311, 69, 22);
		LunchPanal.add(lblNewLabel_6_1);
		
		JTextArea textAreaL_drink = new JTextArea();
		textAreaL_drink.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaL_drink.setBounds(197, 344, 98, 22);
		LunchPanal.add(textAreaL_drink);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 0));
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		panel.setBounds(1031, 210, 333, 404);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1_2 = new JLabel("Dinner");
		lblNewLabel_1_2.setForeground(new Color(128, 0, 0));
		lblNewLabel_1_2.setBounds(125, 9, 82, 31);
		lblNewLabel_1_2.setFont(new Font("Tahoma", Font.BOLD, 25));
		panel.add(lblNewLabel_1_2);
		
		JLabel lblNewLabel_4 = new JLabel("Chicken fried rice");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_4.setBounds(10, 64, 193, 19);
		panel.add(lblNewLabel_4);
		
		JLabel lblNewLabel_4_1 = new JLabel("Egg fried rice");
		lblNewLabel_4_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_4_1.setBounds(10, 103, 182, 31);
		panel.add(lblNewLabel_4_1);
		
		JLabel lblNewLabel_4_2 = new JLabel("Fish fride rice");
		lblNewLabel_4_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_4_2.setBounds(10, 164, 152, 25);
		panel.add(lblNewLabel_4_2);
		
		JLabel lblNewLabel_4_3 = new JLabel("Mix fride rice");
		lblNewLabel_4_3.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_4_3.setBounds(10, 213, 152, 43);
		panel.add(lblNewLabel_4_3);
		
		JLabel lblNewLabel_4_4 = new JLabel("Nasi goreng");
		lblNewLabel_4_4.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_4_4.setBounds(10, 266, 149, 31);
		panel.add(lblNewLabel_4_4);
		
		JTextArea textAreaChickenfriderice = new JTextArea();
		textAreaChickenfriderice.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaChickenfriderice.setBounds(197, 65, 127, 22);
		panel.add(textAreaChickenfriderice);
		
		JTextArea textAreaFishfriderice = new JTextArea();
		textAreaFishfriderice.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaFishfriderice.setBounds(197, 168, 127, 22);
		panel.add(textAreaFishfriderice);
		
		JTextArea textAreaEgefriderice = new JTextArea();
		textAreaEgefriderice.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaEgefriderice.setBounds(197, 110, 127, 22);
		panel.add(textAreaEgefriderice);
		
		JTextArea textAreaMixfriderice = new JTextArea();
		textAreaMixfriderice.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaMixfriderice.setBounds(197, 226, 127, 22);
		panel.add(textAreaMixfriderice);
		
		JTextArea textAreaNasigoreng = new JTextArea();
		textAreaNasigoreng.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaNasigoreng.setBounds(197, 273, 127, 22);
		panel.add(textAreaNasigoreng);
		
		JLabel lblNewLabel_5_2 = new JLabel("Drink");
		lblNewLabel_5_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_5_2.setBounds(22, 323, 76, 22);
		panel.add(lblNewLabel_5_2);
		
		JComboBox D_comboBox = new JComboBox();
		D_comboBox.setFont(new Font("Tahoma", Font.BOLD, 15));
		D_comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select  Drink", "Coke", "Sprite", "EGB", "Cream soda", "Fanta"}));
		D_comboBox.setBounds(10, 355, 132, 21);
		panel.add(D_comboBox);
		
		JLabel lblNewLabel_6_2 = new JLabel("Qty");
		lblNewLabel_6_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel_6_2.setBounds(226, 323, 69, 22);
		panel.add(lblNewLabel_6_2);
		
		JTextArea textAreaD_drink = new JTextArea();
		textAreaD_drink.setFont(new Font("Monospaced", Font.BOLD, 20));
		textAreaD_drink.setBounds(197, 353, 98, 22);
		panel.add(textAreaD_drink);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 255, 0));
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 4));
		panel_1.setBounds(321, 658, 861, 74);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnExsit = new JButton("Exsit");
		btnExsit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExsit.setForeground(new Color(255, 228, 196));
		btnExsit.setBackground(new Color(0, 0, 128));
		btnExsit.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnExsit.setBounds(647, 16, 113, 39);
		panel_1.add(btnExsit);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textAreaNoodls.setText(null);
				textAreaStringhoppers.setText(null);
				textAreaRiceandcurry.setText(null);
				textAreaB_drink.setText(null);
				textAreaChicken.setText(null);
				textAreaEgg.setText(null);
				textAreaVegetable.setText(null);
				textAreaL_drink.setText(null);
				textAreaChickenfriderice.setText(null);
				textAreaEgefriderice.setText(null);
				textAreaFishfriderice.setText(null);
				textAreaMixfriderice.setText(null);
				textAreaNasigoreng.setText(null);
				textAreaD_drink.setText(null);
				B_comboBox.setSelectedItem("Select Drink");
				L_comboBox.setSelectedItem("Select Drink");
				D_comboBox.setSelectedItem("Select Drink");
				
				
			}
		});
		btnReset.setForeground(new Color(255, 228, 196));
		btnReset.setBackground(new Color(0, 0, 128));
		btnReset.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnReset.setBounds(379, 16, 113, 39);
		panel_1.add(btnReset);
		
		JButton btnPlaceOrder = new JButton("Place order");
		btnPlaceOrder.setForeground(new Color(255, 228, 196));
		btnPlaceOrder.setBackground(new Color(0, 0, 128));
		btnPlaceOrder.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnPlaceOrder.setBounds(87, 14, 134, 39);
		panel_1.add(btnPlaceOrder);
	}
}
